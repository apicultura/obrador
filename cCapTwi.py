
try:
	from cConnexio import cConnexio as cCon
	from birdy.twitter import UserClient
	from datetime import datetime as dt
	from datetime import timedelta as td
	from cControl import cControl as cCtr
	from time import sleep
except Exception as e:
	print(e)

class cCapTwi(object):

	def __init__(self,jPet,nExeId,tProjecte):
		self.jPet = jPet
		self.nExeId = nExeId
		self.tProjecte = tProjecte
		self.jParams = jPet['jParams']

		self.cCtrl = cCtr(jPet,nExeId,tProjecte)
		self.cCtrl.pActPetEstat(jPet,'PetProcess')

	def pExecuta(self):
		
		# tenir el codi peticio en una variable
		tPetId = str(self.jPet['_id'])

		print('Dins pExecuta...')
		
		# el teu codi!
		print('Claus...')
		self.oClaus = self.fClaus(self.jParams['tRest'])
		self.cClient = UserClient(self.oClaus['ConsumerKey'],self.oClaus['ConsumerSecret'], self.oClaus['AccessToken'], self.oClaus['AccessTokenSecret'] )
		print('Peticio...')
		oResposta = self.cClient.api.search.tweets.get(q=self.jParams['tQ'])
		print('Gotcha! Insertant...')
		
		laData = oResposta.data
		cCon('obrador',self.nExeId,self.tProjecte).fInsertarUn('destProv',laData)
		print('Insertat!...')

		print('----------- DATA ---------------------')
		print(type(oResposta.data))
		for r in oResposta.data:
			print(r)
			#print(oResposta[r])
		for s in oResposta.data['statuses']:
			#print(s)
			print('insertat un altre!!!')

		print('----------- HEADERS ---------------------')
		print(oResposta.headers)
		for h in oResposta.headers:
			print(h)
			#print(oResposta[r])

		print('Acabant...')

		# actualitzar l'estat de la peticio abans d'acabar
		self.cCtrl.pActPetEstat(self.jPet,'PetFinExit')

	def fClaus(self,tRest):

		jCond = { 'tRest': tRest }
		jCanvi = { '$inc': { 'nQuedenUsr': -1, 'nQuedenApp': -1 } }
		jResultat = cCon('obrador',self.nExeId,self.tProjecte).fConsIAct('CapTwiSit',jCond,jCanvi)

		jCond2 = { 'config' : 'claus' }
		jLim = { 'data.'+jResultat['tClau']: 1}
		jResultat2 = cCon('obrador',self.nExeId,self.tProjecte).fConsultaLim('CapTwiCon',jCond2,jLim)
		
		return jResultat2[0]['data'][jResultat['tClau']]

















"""
		jCond = {
				'query': {
				'tRest': tRest,
				'nQuedenUsr': { '$gt': 0 },
				'nQuedenApp': { '$gt': 0 },
				'dUltima': { '$gte' : 'new ISODate("2016-01-12T20:15:31Z")' }
			},
			'update': { '$inc': { 'nQuedenUsr': -1, 'nQuedenApp': -1 }, '$currentDate': {'lastModified': { '$type': 'timestamp'} } },
			'new': True,
			'fields': { 'tClau': 1 }
		}





class cCapTwRest(object):

	def __init__(self, tUsuariNom, tProjecteNom, tCredencials, nExecucioId):

		try:
			self.tUsuariNom = tUsuariNom
			self.tProjecteNom = tProjecteNom
			self.tCredencials = tCredencials
			self.nEstat = 100
			self.nExecucioId = nExecucioId
			self.bSeguir = True

			self.lClients = {}

			self.conCon = cConnexio(self.tCredencials,self.nExecucioId,self.tUsuariNom)

			self.pAgafarConfiguracio()

			print('AAAAAQUIIIIII LA DAAAATTTTAAAAAA')
			oData = self.fExecucioPeticio(2,'user_timeline','screen_name=\'orpuig\'&count=2')
			print('----------------------')
			print('----------------------')
			print('----------------------')
			print('AAAAAQUIIIIII LA DAAAATTTTAAAAAA ARRRAA SSIIIII')
			print(len(oData.data))
			print(oData.data[0])
			print(oData.data)
		except Exception as e:
			print(e)

	def pAgafarConfiguracio(self):
		try:
			print('Aqui agafem la config de cCapTwRest')
			aqui es declara self.nMigdiada

		except Exception as e:
			print(e)

	def pExecutar(self):
		try:

			print('Executem captura de twitter via rests!')
			while self.bSeguir:

				lPeticions = {}
				lPeticions = self.fPeticionsPendents()

				# paralelitzant amb algo que no sigui el threading, potser el Pool (si funciona)
				for lPet in lPeticions:
					print('FOR DE LES PETICIONS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
					# self.pResoldrePeticio(lPeticions['nPeticioId'])
					self.pResoldrePeticio(2)
				#sleep(self.nMigdiada)

		except Exception as e:
			print('cCapTwRest.pExecutar: error no controlat al executar la captura de twitter rest!')
			print(e)
			raise

	def pDecidirSiSeguir(self):
		try:
			print('Aqui decidim si seguim o no')
		except Exception as e:
			print('cCapTwRest.pDecidirSiSeguir: error no controlat')
			print(e)

	def fPeticionsPendents(self):
		
		try:
			sPetPend = 'SELECT * FROM TW_PET_REST p '
			sPetPend = sPetPend + 'INNER JOIN OBRADOR_PROJECTES pj ON pj.projecte_id = p.projecte_id '
			sPetPend = sPetPend + 'INNER JOIN TW_PET_ESTAT e ON e.estat_id = p.estat_id '
			sPetPend = sPetPend + 'WHERE pj.projecte_nom = "' + self.tProjecteNom + '" '
			sPetPend = sPetPend + 'AND p.hist = 0 '
			sPetPend = sPetPend + 'AND p.data_inici < CURDATE() '
			sPetPend = sPetPend + 'AND p.data_final > CURDATE() '
			sPetPend = sPetPend + 'AND e.estat_nom = "pet_pendent"'

			lPeticions = {}
			lPeticions = self.conCon.fConsultar(sPetPend)


			return lPeticions

		except Exception as e:
			print('cCapTwRest.fPeticionsPendents: error no controlat al buscar peticions pendents')
			print(e)

	def fAgafarClausDisponibles(self,tRest):
		
		try:
			sClaus = 'SELECT c.con_key, c.con_sec, c.acc_tok, c.acc_tok_sec '
			sClaus = sClaus + 'FROM TW_PET_CLAUS c '
			sClaus = sClaus + 'INNER JOIN TW_CLAUS_SITUACIONS s ON s.clau_id = c.clau_id '
			sClaus = sClaus + 'INNER JOIN TW_PET_REST l ON l.rest = s.rest '
			sClaus = sClaus + 'INNER JOIN TW_CLAUS_GRUPS g ON g.clau_id = c.clau_id '
			sClaus = sClaus + 'WHERE s.rest_nom = "' + tRest + '" '
			sClaus = sClaus + 'AND ( s.cont < l.limit_usr OR date() > s.reset_data ) '
			sClaus = sClaus + 'AND g.projecte_nom = "' + self.tProjecteNom + '"'

			lClau = self.conCon.fConsultar(sClaus)

			# aqui no es nomes agafar clau, tambe s'ha d'actualitzar la situacio de la clau
			#	podriem posar la query en una funcio del MySql i aixi que quan l'agafi tambe s'actualitzi.
			#	o fer consultes bloquejants o jo que se... pensar-ho be, perque no es tonteria!
			return lClau
		
		except Exception as e:
			print('---- Error cCapTwRest.init: ' + str(e))
			print(e)

	def pResoldrePeticio(self,nPeticioId):
		
		try:
			sAgafarPeticio = 'SELECT A, B, C, ... FROM TW_PET_REST WHERE peticio_id = ' + str(nPeticioId)
			lPet = self.conCon.fConsultar(sAgafarPeticio)
			
			lPet['nPeticioId'] = 2
			lPet['tTipus'] = 'unica'
			lPet['tRest'] = 'user_timeline'
			lPet['tParam'] = 'screen_name=\'orpuig\''
			lPet['tDest'] = 'predet' # si es predeterminat va a ../[proj]/captures/tw_[nPeticioId].json

			# self.pActualitzarPetEstat(lPet['nPeticioId'],57)

			if lPet['tTipus'] == 'recursiva_tuits' or lPet['tTipus'] == 'recursiva_llistes':
				print('ale')
				self.pExecucioPeticioRecursiva(lPet['tRest'],lPet['tParam'])

			elif lPet['tTipus'] == 'streaming':
				print('ale')
				# fotre-li aqui un streaming buenu
			elif lPet['tTipus'] == 'unica':
				print('FEM LA PETICIO UNICAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA')
				oData = self.fExecucioPeticio(lPet['nPeticioId'],lPet['tRest'],lPet['tParam'])
				# self.pActualitzarPetComentari(nPeticioId,oData.headers) - MA O MENO, la idea es posar algun header!
				print('----------------------')
				print('----------------------')
				print('----------------------')
				print('AAAAAQUIIIIII LA DAAAATTTTAAAAAA')
				print(len(oData.data))
				# fer la peticio i punt

				self.pGuardarData

			# ara decidir si aqui posem la data en un arxiu o a BaseDades...

		except Exception as e:
			print('----- Error cCapTwRest.pResoldrePeticio: ' + str(e))
			print(e)
			print('-----------------------')
			print('-----------------------')

	def pExecucioPeticioRecursiva(self,tRest,tParam):
		try:
			# val, la recursiva sense canviar el estat de la peticio, en canviara els params a base de dades i executara la peticio
			# s'ha de fer un if perque depen de quins Rests son recursius buscant el element (tuit) o cursor (llistes)
			bRecursiu = True
			while bRecursiu:
				# deu mil histories
				nLimit = 100 # s'ha de trobar consultant la TW_REST
				oResposta = self.fExecucioPeticio(tRest,tParam)
				
				if len(oResposta.Data) == 0 or len(oResposta.Data) < nLimit:
					bRecursiu = False
				# deu mil histories
				if tRest == 'TuitsEndarrere':
					nTuitIdNova = self.fTuitMesAntic(oResposta.Data)
					tParam = ''
				elif tRest == 'TuitsEndarrere':
					nTuitIdNova = self.fTuitMesNou(oResposta.Data)
					tParam = ''
				elif tRest == 'Llistes':
					nCursorNou = se.f.fCursorSeguent(oResposta.Data)
					tParam = ''

		except Exception as e:
			print(e)

	def fCursorSeguent(self,oData):
		return oData['next_cursor']

	def fTuitMesAntic(self,oData):
		print('el mes antic!')
		# fer un min(nTuitId)
		return nTuitId

	def fTuitMesNou(self,oData):
		print('el mes nou!')
		# fer un max(nTuitId)
		return nTuitId

	def fExecucioPeticio(self,nPeticioId,tRest,tParam):
		try:
			cClient = self.fCrearClients(nPeticioId,tRest)
			print('aaaaaaPPPPPUNNNNT DEEEeeeEeEeE')
			
			# treure
			# treure
			# treure
			CONSUMER_KEY = 'WMZ9RuEWM2uEwmyxgWviTilaH'
			CONSUMER_SECRET = 'wtJFbOtuigkDjxzsmyVAWUZ2EWv69TrIQoBQw48HNRzJm9m7uy'
			ACCESS_TOKEN = '301370226-0nCXgmm3RFo4nWHyIN3bGqq2vUmi5CgnlpTwqniG'
			ACCESS_TOKEN_SECRET = 'ukpvSFLzjutM2l1o2dfnAqtaTm25oXgBBjScsbIrBC64H'

			cClient = UserClient(CONSUMER_KEY,CONSUMER_SECRET, ACCESS_TOKEN,ACCESS_TOKEN_SECRET)
			# treure

			#screen_name ='orpuig'
			oResposta = cClient.api.statuses.user_timeline.get(screen_name='orpuig')
			#lRest = tRest.split('/')
			#oResposta = cClient.api[lRest[0]][tRest[1]].get(screen_name='orpuig') # mirar com es fa dinamic els params del GET
			return oResposta
		except Exception as e:
			print('---- Error cCapTwRest.fExecucioPeticio: ' + str(e))
			print(e)
			print('--------------------------------------------')
			print('--------------------------------------------')

	def pActualitzarPetEstat(self,nPeticioId,nEstatId):
		try:
			sActualitzaEstat = 'UPDATE TW_PET_REST SET nEstatId = ' + str(nEstatId) + ' WHERE nPeticioId = ' + str(nPeticioId)
			self.conCon.pExecutar(sActualitzaEstat)
		except Exception as e:
			print('---- Error cCapTwRest.pActualitzarPetEstat: ' + str(e))
			print(e)
			print('----------------------------')
			print('----------------------------')

	def pActualitzarPetComentari(self,nPeticioId,tComentari):
		try:
			sActualitzaComent = 'UPDATE TW_PET_REST SET tComent = tComent || \'' + tComentari + '\' WHERE nPeticioId = ' + str(nPeticioId)
			self.conCon.pExecutar(sActualitzaComent)
		except Exception as e:
			print('---- Error cCapTwRest.pActualitzarPetComentari: ' + str(e))
			print(e)
			print('----------------------------')
			print('----------------------------')

	def fCrearClients(self,nPeticioId,tRest):
		try:
			print('CCCCC entrem a fCrearClients --------------------')
			#lClau = self.fAgafarClausDisponibles(lPet['tRest'])
			lClau = ['WMZ9RuEWM2uEwmyxgWviTilaH','wtJFbOtuigkDjxzsmyVAWUZ2EWv69TrIQoBQw48HNRzJm9m7uy','301370226-0nCXgmm3RFo4nWHyIN3bGqq2vUmi5CgnlpTwqniG','ukpvSFLzjutM2l1o2dfnAqtaTm25oXgBBjScsbIrBC64H']
			#self.lClients[nPeticioId] = UserClient(lClau)
			self.lClients[nPeticioId] = UserClient('WMZ9RuEWM2uEwmyxgWviTilaH','wtJFbOtuigkDjxzsmyVAWUZ2EWv69TrIQoBQw48HNRzJm9m7uy','301370226-0nCXgmm3RFo4nWHyIN3bGqq2vUmi5CgnlpTwqniG','ukpvSFLzjutM2l1o2dfnAqtaTm25oXgBBjScsbIrBC64H')
			cClient = UserClient(lClau)
			cClient = UserClient('WMZ9RuEWM2uEwmyxgWviTilaH','wtJFbOtuigkDjxzsmyVAWUZ2EWv69TrIQoBQw48HNRzJm9m7uy','301370226-0nCXgmm3RFo4nWHyIN3bGqq2vUmi5CgnlpTwqniG','ukpvSFLzjutM2l1o2dfnAqtaTm25oXgBBjScsbIrBC64H')
			
			CONSUMER_KEY = 'WMZ9RuEWM2uEwmyxgWviTilaH'
			CONSUMER_SECRET = 'wtJFbOtuigkDjxzsmyVAWUZ2EWv69TrIQoBQw48HNRzJm9m7uy'
			ACCESS_TOKEN = '301370226-0nCXgmm3RFo4nWHyIN3bGqq2vUmi5CgnlpTwqniG'
			ACCESS_TOKEN_SECRET = 'ukpvSFLzjutM2l1o2dfnAqtaTm25oXgBBjScsbIrBC64H'

			cClient = UserClient(CONSUMER_KEY,CONSUMER_SECRET, ACCESS_TOKEN,ACCESS_TOKEN_SECRET)

			return cClient

		except Exception as e:
			print('---- Error cCapTwRest.fCrearClients: ' + str(e))
			print(e)
			print('----------------------------')
			print('----------------------------')




















per tant, hem de diferenciar entre les peticions de un sol cop, o les recursives...

dada de birdy:
	response.data
	response.algu_url
	response.headers ( ehhh, aixo si que molaaaaa )


CONSUMER_KEY = 'WMZ9RuEWM2uEwmyxgWviTilaH'
CONSUMER_SECRET = 'wtJFbOtuigkDjxzsmyVAWUZ2EWv69TrIQoBQw48HNRzJm9m7uy'
ACCESS_TOKEN = '301370226-0nCXgmm3RFo4nWHyIN3bGqq2vUmi5CgnlpTwqniG'
ACCESS_TOKEN_SECRET = 'ukpvSFLzjutM2l1o2dfnAqtaTm25oXgBBjScsbIrBC64H'

client = UserClient(CONSUMER_KEY,CONSUMER_SECRET,ACCESS_TOKEN,ACCESS_TOKEN_SECRET)

# response = client.api.users.show.get(screen_name='orpuig')
response = client.api.statuses.user_timeline.get(screen_name='orpuig',count=4)

laData = response.data

print('--------------------------')
for d in laData:
	#print(d)
	#print(str(d) + ': ' + str(laData[d]))
	print('--------------------------')
print('--------------------------')

print('llargada: ' + str(len(laData)))

print(laData[0])

for m in laData[0]:
	print('////////////////////////////////////////////////')
	print('////////////////////////////////////////////////')
	print(m)
	print(laData[0][m])
	print('////////////////////////////////////////////////')
	print('////////////////////////////////////////////////')


t = 0
while t < len(laData):
	tuit = laData[t]


"""
