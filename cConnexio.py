# posar try/exception

from pymongo import MongoClient
from pymongo import ReturnDocument

class cConnexio(object):

	def __init__(self, tDb, tExeId, tProjecte):
		self.tDb = tDb
		self.tExeId = tExeId
		self.tProjecte = tProjecte
		self.pConnectar()

	def pConnectar(self):
		self.conClient = MongoClient()
		self.conDb = self.conClient[self.tDb]

	def pDesconnectar(self):
		# delete self.conClient
		pass

	def fInsertarUn(self,tColec,jDoc):
		jResultat = self.conDb[tColec].insert_one(jDoc)
		return jResultat

	def fInsertarVaris(self,tColec,jDocs):
		jResultat = self.conDb[tColec].insert_many(jDocs)
		return jResultat

	def fEliminarUn(self,tColec,jCond):
		jResultat = self.conDb[tColec].delete_one(jCond)
		return jResultat

	def fActualitzar(self,tColec,jCond,jCanvi):
		jResultat = self.conDb[tColec].update(jCond,jCanvi)
		return jResultat

	def fSusbstituir(self,tColec,jCond,jNou):
		jResultat = self.conDb[tColec].replace(jCond,jNou)
		return jResultat

	def fConsulta(self,tColec,jCond):
		jResultat = self.conDb[tColec].find(jCond)
		return jResultat

	def fConsultaLim(self,tColec,jCond,jLim):
		jResultat = self.conDb[tColec].find(jCond,jLim)
		return jResultat

	def fConsIAct(self,tColec,jCond,jCanvi):
		jResultat = self.conDb[tColec].find_one_and_update(jCond,jCanvi,projection=None,sort=None,return_document=ReturnDocument.AFTER)
		return jResultat

	def fConsCount(self,tColec,jCond):
		jResultat = self.conDb[tColec].find(jCond).count()
		return jResultat

	def fColletions(self):
		jResultat = self.conDb.collection_names(include_system_collections=False)
		return jResultat

	def fEliminarCollection(self,tColec):
		jResultat = self.conDb[tColec].drop()
		return jResultat


