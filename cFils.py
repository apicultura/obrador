import threading
import importlib

class cFils(threading.Thread):

	def __init__(self,tObj,tMet,jPet,nExeId,tProjecte):
		threading.Thread.__init__(self)
		self.tObj = tObj
		self.tMet = tMet
		self.jPet = jPet
		self.nExeId = nExeId
		self.tProjecte = tProjecte

	def run(self):
		cImp = importlib.import_module(self.tObj)
		cObj = getattr(cImp,self.tObj)
		cIns = cObj(self.jPet,self.nExeId,self.tProjecte)
		fMet = getattr(cIns,self.tMet)
		fMet()


#fil = {}
#fil[1] = cFils('whatever','fer5',1)
#fil[2] = cFils('whatever','fer5',2)
#fil[3] = cFils('whatever','fer5',3)

#for f in fil:
#	fil[f].start()



