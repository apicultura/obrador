
from cConnexio import cConnexio as cCon
from datetime import datetime as dt
from datetime import timedelta as td

conDB = cCon('obrador',0,'Twi')

conDB.fEliminarCollection('CapTwiCon')
conDB.fEliminarCollection('CapTwiSit')

jConfig = {
	'config': 'postRests',
	'data': {
		'tAppUsr': 'Usr'
	}
}

jPostRests = {
	'config': 'postRests',
	'data': {}
}

jClaus = {
	'config': 'claus',
	'data': {
		'orpuig_exem': {
			'tId': 'orpuig_exem',
			'ConsumerKey': 'WMZ9RuEWM2uEwmyxgWviTilaH',
			'ConsumerSecret': 'wtJFbOtuigkDjxzsmyVAWUZ2EWv69TrIQoBQw48HNRzJm9m7uy',
			'AccessToken': '301370226-0nCXgmm3RFo4nWHyIN3bGqq2vUmi5CgnlpTwqniG',
			'AccessTokenSecret': 'ukpvSFLzjutM2l1o2dfnAqtaTm25oXgBBjScsbIrBC64H'
		},
		'orpuig_exem2': {
			'tId': 'orpuig_exem2',
			'ConsumerKey': 'WMZ9RuEWM2uEwmyxgWviTilaH',
			'ConsumerSecret': 'wtJFbOtuigkDjxzsmyVAWUZ2EWv69TrIQoBQw48HNRzJm9m7uy',
			'AccessToken': '301370226-0nCXgmm3RFo4nWHyIN3bGqq2vUmi5CgnlpTwqniG',
			'AccessTokenSecret': 'ukpvSFLzjutM2l1o2dfnAqtaTm25oXgBBjScsbIrBC64H'
		}
	}
}


jGetRests = {
	'config': 'getRests',
	'data': {
		'searchTweets': {
			'tId': 'searhTweets', 'tCod': 'SeaTw',
			'nLimUsr': 100, 'nLimApp': 200,
			'tUrl': 'api.Twitter...',
			'jParams': {
				'q': {},
				'geocode': {},
				'lang': {},
				'locale': {},
				'result_type': {},
				'count': {},
				'until': {},
				'since_id': {},
				'max_id': {},
				'include_entities': {},
				'callback': {}
			},
			'jExemple': {},
			'tDesc': 'Twitter, xarxa social de microblogging'
		},
		'statusesUserTimeline': {
			'tId': 'statusesUserTimeline', 'tCod': 'UsrTl',
			'nLimUsr': 100, 'nLimApp': 200,
			'tUrl': 'api.Twitter.com...',
			'jParams': {},
			'jExemple': {},
			'tDesc': 'Twitter, xarxa social de microblogging' 
		},
		'statusesHomeTimeline': {
			'tId': 'statusesHomeTimeline', 'tCod': 'HmeTl',
			'nLimUsr': 100, 'nLimApp': 200,
			'tUrl': 'api.Twitter.com...',
			'jParams': {},
			'jExemple': {},
			'tDesc': 'Twitter, xarxa social de microblogging' 
		},
		'friendsIds': {
			'tId': 'FriendsIds', 'tCod': 'FrIds',
			'nLimUsr': 100, 'nLimApp': 200,
			'tUrl': 'api.Twitter.com...',
			'jParams': {},
			'jExemple': {},
			'tDesc': 'Twitter, xarxa social de microblogging' 
		},
		'friendsList': {
			'tId': 'FriendsList', 'tCod': 'FrLst',
			'nLimUsr': 100, 'nLimApp': 200,
			'tUrl': 'api.Twitter.com...',
			'jParams': {},
			'jExemple': {},
			'tDesc': 'Twitter, xarxa social de microblogging' 
		},
		'followersIds': {
			'tId': 'FollowersIds', 'tCod': 'FoIds',
			'nLimUsr': 100, 'nLimApp': 200,
			'tUrl': 'api.Twitter.com...',
			'jParams': {},
			'jExemple': {},
			'tDesc': 'Twitter, xarxa social de microblogging' 
		},
		'followersList': {
			'tId': 'FollowersList', 'tCod': 'FoLst',
			'nLimUsr': 100, 'nLimApp': 200,
			'tUrl': 'api.Twitter.com...',
			'jParams': {},
			'jExemple': {},
			'tDesc': 'Twitter, xarxa social de microblogging'
		 }
	}
}


conDB.fInsertarUn('CapTwiCon',jGetRests)
conDB.fInsertarUn('CapTwiCon',jPostRests)
conDB.fInsertarUn('CapTwiCon',jClaus)


jSituacions = {}
i = 0
for r in jGetRests['data']: 
	for c in jClaus['data']:
		jSituacions[str(i)] = {
			'tRest': r, 'tClau': c,
			'nQuedenUsr': jGetRests['data'][r]['nLimUsr'],
			'nQuedenApp': jGetRests['data'][r]['nLimApp'],
			'dUltima': dt.now() - td(minutes=15)
		}
		i += 1

for s in jSituacions:
	conDB.fInsertarUn('CapTwiSit',jSituacions[s])


