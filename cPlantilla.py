
from time import sleep
from cConnexio import cConnexio as cCon
from cControl import cControl as cCtr

class cPlantilla(object):

	def __init__(self,jPet,nExeId,tProjecte):
		self.jPet = jPet
		self.nExeId = nExeId
		self.tProjecte = tProjecte
		self.jParams = jPet['jParams']

		self.cCtrl = cCtr(jPet,nExeId,tProjecte)
		self.cCtrl.pActPetEstat(jPet,'PetProcess')

	def pExecuta(self):
		
		# tenir el codi peticio en una variable
		tPetId = str(self.jPet['_id'])

		# el teu codi!

		# actualitzar l'estat de la peticio abans d'acabar
		self.cCtrl.pActPetEstat(self.jPet,'PetFinExit')

