
from cConnexio import cConnexio as cCo
from datetime import datetime as dt

conDB = cCo('obrador','0','obrador')

conDB.fEliminarCollection('con')
conDB.fEliminarCollection('exe')
conDB.fEliminarCollection('pet')
conDB.fEliminarCollection('log')
conDB.fEliminarCollection('err')

jErrors = {
	'config': 'errors',
	'data': {
		'ErrExeNoCont': { 'tId': 'ErrExeNoCont', 'nCodErr': 10 },
		'ErrPetNoCont': { 'tId': 'ErrPetNoCont', 'nCodErr': 100 },
		'Err1': { 'tId': '', 'nCodErr': 1 }
	}
}

jEstats = {
	'config': 'estats',
	'data': {
		'PetPendent': { 'tId': 'PetPendent', 'nEstat': 10, 'tDesc': 'Les peticions que poden ser gestioandes' },
		'PetAgafada': { 'tId': 'PetAgafada', 'nEstat': 20, 'tDesc': 'Les peticions que les ha agafat el gestor de peticions' },
		'PetProcess': { 'tId': 'PetProcess', 'nEstat': 30, 'tDesc': 'Les peticions que ja s\'estan processant al seu metode' },
		'PetFinExit': { 'tId': 'PetFinExit', 'nEstat': 40, 'tDesc': 'Les peticions que han finalitzat amb exit' },
		'PetErrDesc': { 'tId': 'PetErrDesc', 'nEstat': 50, 'tDesc': 'Les peticions que poden ser gestioandes' },
		'ExeIniciat': { 'tId': 'ExeIniciat', 'nEstat': 1000, 'tDesc': 'La execucio s\'ha iniciat' },
		'ExeFinalit': { 'tId': 'ExeFinalit', 'nEstat': 1010, 'tDesc': 'La execucio ha finalitzat' },
		'ExeErrDesc': { 'tId': 'ExeErrDesc', 'nEstat': 1500, 'tDesc': 'La execucio ha finalitzat' }
	}
}

jProjectes = {
	'config': 'projectes',
	'data': {
		'SobTech': { 'tId': 'SobTech', 'lUsuaris': [ 'catbru', 'etorius', 'orpuig' ], 'tDesc': 'Ajuntant anarcos exiliats a Cat' },
		'PornNet': { 'tId': 'PornNet', 'lUsuaris': [ 'catbru' ], 'tDesc': 'Enllasant les ganes de que arribi la VR al porno' },
		'PezWanda': { 'tId': 'PezWanda', 'lUsuaris': [ 'etorius' ], 'tDesc': 'Mortalitat de peixos al principat creuat per la prob de tenir la seu de un cartel a 2km' }
	}
}

jUsuaris = { 
	'config': 'usuaris',
	'data': {
		'orpuig': { 'tId': 'orpuig', 'tDesc': 'El que no es droga' },
		'catbru': { 'tId': 'catbru', 'tDesc': 'Porno multipestanya. EstropeaoRotoPerfecto' },
		'etorius': { 'tId': 'etorius', 'tDesc': 'Cartel animalista de pastis granes' }
	}
}

jFonts = { 
	'config': 'fonts',
	'data': {
		'twitter': { 'tId': 'twitter', 'tCod': 'twi', 'tDesc': 'Twitter, xarxa social de microblogging' },
		'facebook': { 'tId': 'facebook', 'tCod': 'fbk', 'tDesc': 'Facebook, xarxa social de murs' },
		'instagram': { 'tId': 'instagram', 'tCod': 'ing', 'tDesc': 'Instagram, xarxa social de microblogging fotografic' },
		'viquipedia': { 'tId': 'viquipedia', 'tCod': 'viq', 'tDesc': 'Viquipedia, enciclopedia virtual collaborativa' },
		'youtube': { 'tId': 'youtube', 'tCod': 'you', 'tDesc': 'Youtube, xarxa social de videos' },
		'linkedin': { 'tId': 'linkedin', 'tCod': 'lin', 'tDesc': 'LinkedIn, xarxa social de mur professional' },
		'gmail': { 'tId': 'gmail', 'tCod': 'gma', 'tDesc': 'Gmail, servei de correu electronic' },
		'idescat': { 'tId': 'idescat', 'tCod': 'ide', 'tDesc': 'Idescat, institut de estadistica de Catalunya' },
		'icc': { 'tId': 'icc', 'tCod': 'icc', 'tDesc': 'ICC, institut cartografic de Catalunya' },
		'gmaps': { 'tId': 'gmaps', 'tCod': 'map', 'tDesc': 'GoogleMaps, servei cartografic de google' },
		'gcalendar': { 'tId': 'gcalendar', 'tCod': 'cal', 'tDesc': 'GoogleCalendar, servei de calendari de google' },
		'github': { 'tId': 'github', 'tCod': 'git', 'tDesc': 'Github, xarxa social de desenvolupadors' },
		'spotify': { 'tId': 'spotify', 'tCod': 'spo', 'tDesc': 'Spotify, servei de reproduccio de musica streaming' },
		'wordpress': { 'tId': 'wordpress', 'tCod': 'wpr', 'tDesc': 'Wordpress, xarxa de blogs' }
	}
}

jExeConfig = {
	'config': 'ExeConfig',
	'data': {
		'nMaxVoltesExe': 0,
		'nExeSleep': 1
	}
}

jPetConfig = {
	'config': 'PetConfig',
	'data': {
		'nMaxVoltesPet': 5,
		'nPetAgafar': 2,
		'nPetSleep': 1
	}
}

print('Abans de fer el insert del config!')

conDB.fInsertarUn('con',jErrors)
conDB.fInsertarUn('con',jEstats)
conDB.fInsertarUn('con',jProjectes)
conDB.fInsertarUn('con',jUsuaris)
conDB.fInsertarUn('con',jFonts)
conDB.fInsertarUn('con',jExeConfig)
conDB.fInsertarUn('con',jPetConfig)


print('Anem a posar algunes peticions aviam!')

jParams1 = { 'nSleep': 1, 'tRest': 'searchTweets', 'tQ': 'catalunya', 'tDesti': 'dest' }
jPet1 = { 'tProjecte': 'SobTech', 'tLib': 'cCapTwi', 'tMet': 'pExecuta', 'tData': dt.now(), 'jParams': jParams1, 'nEstat': jEstats['data']['PetPendent']['nEstat'], 'lLog': [] }

jParams2 = { 'nSleep': 2 }
jPet2 = { 'tProjecte': 'SobTech', 'tLib': 'cPlantilla', 'tMet': 'pExecuta', 'tData': dt.now(), 'jParams': jParams2, 'nEstat': jEstats['data']['PetPendent']['nEstat'], 'lLog': [] }

jParams3 = { 'nSleep': 3 }
jPet3 = { 'tProjecte': 'SobTech', 'tLib': 'cPlantilla', 'tMet': 'pExecuta', 'tData': dt.now(), 'jParams': jParams3, 'nEstat': jEstats['data']['PetPendent']['nEstat'], 'lLog': [] }

jParams4 = { 'nSleep': 4 }
jPet4 = { 'tProjecte': 'SobTech', 'tLib': 'cPlantilla', 'tMet': 'pExecuta', 'tData': dt.now(), 'jParams': jParams4, 'nEstat': jEstats['data']['PetPendent']['nEstat'], 'lLog': [] }

jParams5 = { 'nSleep': 1 }
jPet5 = { 'tProjecte': 'PornNet', 'tLib': 'cPlantilla', 'tMet': 'pExecuta', 'tData': dt.now(), 'jParams': jParams5, 'nEstat': jEstats['data']['PetPendent']['nEstat'], 'lLog': [] }

jParams6 = { 'nSleep': 3 }
jPet6 = { 'tProjecte': 'PornNet', 'tLib': 'cPlantilla', 'tMet': 'pExecuta', 'tData': dt.now(), 'jParams': jParams6, 'nEstat': jEstats['data']['PetPendent']['nEstat'], 'lLog': [] }

jPets = [ jPet1, jPet2, jPet3, jPet4, jPet5, jPet6 ]

conDB.fInsertarVaris('pet',jPets)

